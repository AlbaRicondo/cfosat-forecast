#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import os.path as op
import glob

# arrays
import numpy as np
import pandas as pd
import xarray as xr
import datetime

# plot
import cmocean
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.axes as maxes

# cartopy maps
import cartopy.crs as ccrs
#from cartopy.io.shapereader import Reader
#from cartopy.feature import ShapelyFeature

# dependencies
from lib.config import *

'''Library to plot satellite tracks'''

def map_init(figsize):
    fig = plt.figure(figsize=figsize)
    gs = fig.add_gridspec(1, 1)
    ax = fig.add_subplot(gs[0, 0], projection=ccrs.PlateCarree(central_longitude=180))
    return(fig, ax)

def map_settings(ax):
    '''Add aspect settings to maps'''
    
    ax.patch.set_facecolor('cornflowerblue')
    ax.patch.set_alpha(0.2)
    ax.add_feature(shape_feature_l)
    return(ax)

def add_colorbar(ax):
    '''Add vertical ax colorbar to fig'''
    
    divider = make_axes_locatable(ax)
    loc = mdates.AutoDateLocator()
    axc = divider.new_horizontal(size="%3", pad=0.1, axes_class=plt.Axes)
    return(axc, loc)

def global_map(lon, lat, estela_mask, title, figsize):

    fig, ax = map_init(figsize)
    ax = map_settings(ax)
    #estela_mask.plot(ax=ax, transform=ccrs.PlateCarree(), cmap='Blues', add_colorbar=False)
    ax.scatter(lon, lat, c='darkslateblue', s=1, transform=ccrs.PlateCarree(), zorder=2, label= 'CFOSAT observations')
    ax.scatter(site_lon, site_lat, s=35, c='r', transform=ccrs.PlateCarree(), zorder=3, label = site)
    ax.set_extent([-180, 180, -90, 90])
    ax.set_title(title, fontsize=20)
    plt.legend(loc='upper left')

    return(ax)

def site_map(lon, lat, title, figsize):

    fig, ax = map_init(figsize)
    ax = map_settings(ax)
    ax.scatter(lon, lat, c='k', s=3, transform=ccrs.PlateCarree())
    ax.set_extent([site_lon-15, site_lon+15, site_lat-15, site_lat+15])
    ax.set_title(title, fontsize=10)

    return(ax)

def timepass_map(data, title, figsize):
    
    # create datetime colorbar
    t = mdates.date2num(data.time.values)
    ndays = (data.time.max() - data.time.min()).days
    cmap = plt.cm.rainbow  
    cmaplist = [cmap(i) for i in range(cmap.N)]
    bounds = np.linspace(t.min(), t.max(), ndays+1)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
   
    fig, ax = map_init(figsize)
    ax = map_settings(ax)
    ax.add_feature(shape_feature_l)

    im = ax.scatter(data.longitude.values, data.latitude.values,
                     c=t, s=8, cmap=cmap, norm=norm, transform=ccrs.PlateCarree())

    ax.gridlines(draw_labels=True)
    ax.set_title(title, fontsize=15)

    # plt colorbar
    axc, loc = add_colorbar(ax)
    fig.add_axes(axc)
    plt.colorbar(im, cax=axc, ticks=loc,
                 format=mdates.AutoDateFormatter(loc))

    return(fig)

def estela_map(figsize, title, estela_mask):
    '''Plot ESTELA masked area in a map'''
    
    fig, ax = map_init(figsize)
    ax = map_settings(ax)
    estela_mask.plot(ax=ax, transform=ccrs.PlateCarree(), cmap='Purples', add_colorbar=False, zorder=1)
    ax.scatter(site_lon, site_lat, s=35, c='r', transform=ccrs.PlateCarree(), zorder=2)
    ax.set_title(title, fontsize=12)

    return(fig, ax)
    
    
def attrs_map(lon, lat, array_color, figsize, title, cmap, vmin, vmax):

    fig, ax = map_init(figsize)
    ax = map_settings(ax)
    
    im = ax.scatter(lon, lat, c=array_color, s=10, 
                    cmap=cmap,
                    vmin=vmin, vmax=vmax,
                    transform=ccrs.PlateCarree())
    
    # title
    ax.set_title(title)
    
    # colorbar
    axc, loc = add_colorbar(ax)
    fig.add_axes(axc)
    plt.colorbar(im, cax=axc)
    return(fig, ax)
    
    
def scatter_forecast(data, cmap, figsize):

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    im = ax.scatter(data.date, data.bearing1, c=data.Hs, cmap=cmap)
    
    # axis
    ax.set_xlim(data.date.values.min(), data.date.values.max())
    ax.set_ylim(0,360)
    ax.set_ylabel('Dir (º)')
    ax.set_xlabel('Arrival date')
    plt.xticks(rotation=45)
    
    # colorbar
    axc, loc = add_colorbar(ax)
    fig.add_axes(axc)
    plt.colorbar(im, cax=axc, label='Hs(m)')
    
    return(fig, ax)


def grid_forecast(data, X, Y, Z, cmap, vmin, vmax, figsize):
    
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    im = ax.pcolor(Y, X, Z, cmap=cmap, vmin=vmin, vmax=vmax)
    
    # axis
    ax.set_xlim(data.time.min(), data.time.max())
    ax.set_ylim(0,360)
    ax.set_ylabel('Dir (º)')
    ax.set_xlabel('Arrival date')
    plt.xticks(rotation=45)

    # colorbar
    axc, loc = add_colorbar(ax)
    fig.add_axes(axc)
    plt.colorbar(im, cax=axc, label='Hs(m)')

    return(fig, ax)
    
    
    
    
    
    
    
    