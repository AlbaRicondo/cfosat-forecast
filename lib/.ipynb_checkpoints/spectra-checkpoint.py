#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as op
import sys

from math import pi
import numpy as np
import pandas as pd
import argparse
import netCDF4 as netcdf
import datetime
import xarray as xr
from datetime import timedelta

from matplotlib import pyplot as plt
from matplotlib.patches import Patch

# bearing
import great_circle_calculator.great_circle_calculator as gcc
from geopy.distance import geodesic

# dependencies
from .config import *

import warnings
warnings.filterwarnings('ignore')



def get_box(lons, lats, lon_x, lat_x):
    """
    Get nearest satellite-box from the study site
    Usage:
        (box, min_d) = get_box(lons, lats, lon_x, lat_x)
    With:
        :param      lons, lats     :track coordinates   
        :param      lon_x, lat_x   :site coordinates                
        :type       rho            :float
        :returns    (box, min_d)   :coordinates in carthesian
        :rtype      (x,y)   :tuple of floats
    """
    min_d = 1000000
    for i in range(len(lons)):
        dist = geodesic((lat_x, lon_x), (lats[i], lons[i])).kilometers
        if dist < min_d:
            min_d = dist
            box = i
    return box, min_d

def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))

def partitions_info(cdf, num_box, nadir_side, beam):
    """
    Print a cell text with the mean partitions parameters
    Usage:
        (cell_text) = partitions_info(lons, lats, lon_x, lat_x)
    With:
        :param      cdf            :netcdf Dataset CFOSAT file 
        :param      nadir_side     :side of the nadir track to analyse (0 : right sides ; 1 :left side)                
        :param      num_box        :float
        :param      beam           :beam choice for spectra (6,8,10,0-combined)
        :returns    cell_text      :mean partitions parameters
        :rtype      ()             :structre
    """

    # Wave parameters of each partition of the combined spectrum (SWH, peak wavelength, peak direction)
    swh_part = cdf.variables['wave_param_part'][0,:,nadir_side,num_box,int((beam-6)/2)]
    dom_wl = cdf.variables['wave_param_part'][1,:,nadir_side,num_box,int((beam-6)/2)]
    dom_azi = cdf.variables['wave_param_part'][2,:,nadir_side,num_box,int((beam-6)/2)]

    swh_part_comb = cdf.variables['wave_param_part_combined'][0,:,nadir_side,num_box]
    dom_wl_comb = cdf.variables['wave_param_part_combined'][1,:,nadir_side,num_box]
    dom_azi_comb = cdf.variables['wave_param_part_combined'][2,:,nadir_side,num_box]


    # for combination of the 3 beams
    if beam == 0 :
        cell_text= pd.DataFrame(
                {
                    'partition': [1,2,3],
                    'SWH(m)':[swh_part_comb[0], swh_part_comb[1], swh_part_comb[2]],
                    'wavelength(m)':[dom_wl_comb[0], dom_wl_comb[1], dom_wl_comb[2]],
                    'direction (°)':[dom_azi_comb[0], dom_azi_comb[1], dom_azi_comb[2]]
                }
            )
    # for one selected beam            
    else :
        cell_text= pd.DataFrame(
                    {
                        'partition': [1,2,3],
                        'SWH(m)':[swh_part[0], swh_part[1], swh_part[2]],
                        'wavelength(m)':[dom_wl[0], dom_wl[1], dom_wl[2]],
                        'direction (°)':[dom_azi[0], dom_azi[1], dom_azi[2]]
                    }
        )

    return(cell_text)


def rawSpectra(p_comb, kspectra, dom_azi,
               posneg=0, box=562,
               n_k=65,
               n_phi=24,
               n_partitions=3):
    """
    Preprocess data to plot 2D direction spectrum for all incidences combined
    Usage:
        specm_comb, mask_comb  = process_pcomb(p_comb, kspectra, parts_comb,
                                                posneg, box,n_k, n_phi, n_partitions)
    With:
        :param p_comb       :2D combined measure spectrum   :L2     :p_combined     :E
        :param kspectra     :wave number vector             :L2     :k_spectra      :E
        :param parts_comb   :mask of detected partitions    :L2     :mask_combined  :E
        :param posneg       :left/right                     :L2     :n_posneg       :E
        :param box          :box number                     :NA     :NA             :E
        :param n_k          :length wave number             :L2     :nk             :E
        :param n_phi        :2*len(phi original)            :NA     :NA             :E
        :param n_partitions :number of detected partitions  :NA     :NA             :E     
        
        :type p_comb        :numpy ma array
        :type kspectra      :float
        :type parts_comb    :float
        :type posneg        :int
        :type box           :int
        :type n_k           :int
        :type n_phi         :int
        :type n_partitions  :int

        :returns    specm_comb  :Mean modulation spectrum combining all incidence beams from 0:360°
        :returns    mask_comb   :Mask for all partitions detected
        :rtype      specm       :numpy.ma
        :rtype      mask_comb   :numpy.ma
    """
    
    # spectre built with phi = 0° at the north
    spec = np.ma.zeros((n_phi, n_k))
    
    # spec == pp_mean[:, phi,:...] only if p_combined not masked else masked
    spec[0:6, :] = np.ma.masked_where(np.ma.getmask(
                   p_comb[:, 5::-1, posneg, box].T) == True,
                   p_comb[:, 5::-1, posneg, box].T)
    spec[6:12, :] = np.ma.masked_where(np.ma.getmask(
                    p_comb[:, 11:5:-1, posneg, box].T) == True,
                    p_comb[:, 11:5:-1, posneg, box].T)
    
    spec[12:18, :] = np.ma.masked_where(np.ma.getmask(
                     p_comb[:, 5::-1, posneg, box].T) == True,
                     p_comb[:, 5::-1, posneg, box].T)
    spec[18:24, :] = np.ma.masked_where(np.ma.getmask(
                     p_comb[:, 11:5:-1, posneg, box].T) == True,
                     p_comb[:, 11:5:-1, posneg, box].T)
    
    # limits inf, limits lambda < 500m
    specm_comb = np.ma.masked_where(spec < -1.e8, spec)
    specm_comb_lim = np.zeros_like(specm_comb)
    
    # limit Wavelength
    #limit_k = (2*pi)/500.0
    #print(specm_comb.shape[0])
    #for i in range(specm_comb.shape[0]):
    #    specm_comb_lim[i] = np.ma.masked_where(kspectra[:] < limit_k, specm_comb[i])
    
    return(specm_comb)


def oneSide(specm_comb_lim, kspectra, parts_comb, dom_azi,
            posneg=0, box=562,
            n_k=65,
            n_phi=24,
            n_partitions=3):
    
    # integer part of number
    original_mask = np.int32(parts_comb[:, :, :, :, :])
    
    # Looking for places where data is masked -- for box = n°box => phi = 0:360:15
    r = np.tile(kspectra[:], n_phi).reshape(n_phi, len(kspectra[:]))
    
    #  phi = 0° at the North
    mask_comb_original = np.zeros((n_partitions, n_phi, n_k))
    mask_comb_original[:, 0:6, :]   = original_mask[:, 5::-1, :, posneg, box].T
    mask_comb_original[:, 6:12, :]  = original_mask[:, 11:5:-1, :, posneg, box].T
    mask_comb_original[:, 12:18, :] = original_mask[:, 5::-1, :, posneg, box].T
    mask_comb_original[:, 18:, :]   = original_mask[:, 11:5:-1, :, posneg, box].T
    
    # convert phi = 0º at the North for the specm_comb_lim
    #spectra = np.zeros((n_phi+1, n_k))
    #spectra[0:6, :]   = specm_comb_lim[5::-1, :]
    #spectra[6:12, :]  = specm_comb_lim[11:5:-1, :]
    #spectra[12:18, :] = specm_comb_lim[5::-1, :]
    #spectra[18:, :]   = specm_comb_lim[11:4:-1, :]   
    
    mask_comb = np.copy(mask_comb_original)
    
    # mask partitions with 180° ambiguity
    ds_spectra = xr.Dataset({'part':[]})
    for part in range(n_partitions):
        ang_part = dom_azi[part]
        
        #if ang_part < 90: ang_east = 90-ang_part
        #elif ang_part > 270: ang_east = (360-ang_part)+90
        #elif (ang_part > 90) & (ang_part < 180): ang_east = 360-(ang_part-90)
        #else: ang_east = (360-ang_part)+90

        angles = np.arange(0,360,15)
        pos = np.tile(range(len(angles)),2)
        place_angle = np.abs(angles-ang_part).argmin()
        array = np.arange(place_angle-6, place_angle+7, 1)
        array = pos[array]
        for p, i in enumerate(array):
            if i < 0:
                array[p] = len(angles)-i
        
        notin = [i for i in range(len(angles)) if i not in array]
        notin = np.sort(notin)
        
        mask_comb[part, notin, :] = 0
        spt = np.copy(specm_comb_lim)*mask_comb[part, :, :]
        
        # return xarray dataset with energy in partitions
        dsi = xr.Dataset(
            {
                "slopeD": (["theta", "k"], spt),
            },
            coords={
                "theta": (angles),
                "k": (r[0].data),
            },
        )
        dsi = dsi.assign_coords(part=part).expand_dims('part')
        ds_spectra = xr.combine_by_coords([ds_spectra, dsi])
  
    return(ds_spectra, mask_comb_original, mask_comb)


def compute_bulkSector(box, posneg, beam, NC_File_L2, min_wavelength, max_wavelength):
    
    # initializations
    cdf = netcdf.Dataset(NC_File_L2)
    time = cdf.variables['time_spec_l2'][:]
    lon = cdf.variables['lon_spec_l2'][:]
    lat = cdf.variables['lat_spec_l2'][:]
    K_SPECTRA = cdf.variables['k_spectra'][:]       # Wave number vector
    PARTI_COMB  = cdf.variables['mask_combined'][:]
    PARTI_BEAM = cdf.variables['mask'][:]
    P_COMBINED = cdf.variables['p_combined'][:]                
    PP_MEAN = cdf.variables['pp_mean'][:]           # 2D mean slope spectrum
    N_KI    = int(32)#int(65)
    N_PHI  = int(12)
    N_PARTITIONS = int(3)
    k_max= round(2*pi/min_wavelength,3)
    k_min= round(2*pi/max_wavelength,3)   
    
    # display spectrum
    if beam == 0:
        beam_title='combined'
    else:
        PARTI_COMB = PARTI_BEAM[:,:,:,:,:,int((beam-6)/2)]
        P_COMBINED = PP_MEAN[:,:,:,:,int((beam-6)/2)]
        beam_title = 'beam '+ str(beam)+'$\degree$'

    # spectra: partitions energy combined 
    dom_azi = cdf.variables['wave_param_part'][2,:,posneg,box,int((beam-6)/2)]
    spectra, ds_spectra, specm_comb, mask_comb = process_pcomb(P_COMBINED, K_SPECTRA, PARTI_COMB, dom_azi, 
                                posneg=posneg, 
                                box=box, n_k=N_KI, n_phi=N_PHI*2, 
                                n_partitions=N_PARTITIONS)
    return(ds_spectra)

def from_Slope_Density(ds, K_SPECTRA):
    
    A = np.ones((np.shape(ds.slopeD)))
    try:
        for i in range(len(ds.theta.values)):
            A[i,:] = K_SPECTRA.data
     
        B = ds.slopeD/A
        
    except:
        A = K_SPECTRA.data
        B = ds.slopeD/A
        
    return(B)
    
def meanParameters(ds, distance, K_SPECTRA):
    '''
    Compute mean wave parameters of directional sector:
    Significant Wave Height - Hs
    Peak Period - T
    Celerity - C
    Travel Time = f(C, distance)
    '''
    
    A = np.ones((np.shape(ds.slopeD)))
    try:
        for i in range(len(ds.theta.values)):
            A[i,:] = K_SPECTRA.data
        kv, tv = np.meshgrid(np.diff(ds.k.values), np.diff(ds.theta.values*(2*pi)/360))
        B = ds.slopeD/A
        Hs = 4*np.sqrt(np.sum(kv*tv*B.values[:-1,:-1]))
        k = ds.k.values[ds.slopeD.argmax(axis=0)[0]]
        
    except:
        A = K_SPECTRA.data
        kv, tv = np.meshgrid(np.diff(ds.k.values), 15*(2*pi)/360)
        B = ds.slopeD/A
        Hs = 4*np.sqrt(np.sum(kv*tv*B.values[:-1]))
        k = ds.k.values[ds.slopeD.argmax(axis=0)]

    g = 9.806
    wl = 2*np.pi/k
    T = np.sqrt(2*np.pi*wl/g)
    C = g*T/(2*np.pi)
            
    Ttime = (distance/C)
    delta = timedelta(seconds=Ttime)
    
    if Hs==0: T=np.nan
    
    return(Hs,T,C,delta)
    

def geoBearing(lon_aux, lat_aux):
    '''
    Get distance and bearing from 2 geographical points
    lon_aux, lat_aux
    Lon, Lat - study point
    '''
    
    if lon_aux < -180: lon_aux = lon_aux+360
    distance = gcc.distance_between_points([lon_aux, lat_aux], [site_lon, site_lat], unit='meters', haversine=True)
    bearing_p1 = gcc.bearing_at_p1([site_lon, site_lat], [lon_aux, lat_aux])
    bearing = gcc.bearing_at_p2([site_lon, site_lat], [lon_aux, lat_aux])
            
    if bearing < 0: bearing = bearing + 360
    if bearing_p1 < 0: bearing_p1 = bearing_p1 + 360

    return(distance, bearing, bearing_p1)













