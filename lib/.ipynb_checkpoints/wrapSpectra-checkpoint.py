#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as op
import sys
import glob

import numpy as np
import pandas as pd
import netCDF4 as netcdf
import datetime
import xarray as xr
from math import pi
import math
from datetime import timedelta

import gc

import warnings
warnings.filterwarnings('ignore')

# dependencies
from lib import spectra, io
from lib.config import *


def mfwam_info(SWIM_L2_NC_file_name, date, nadir_side, num_box, lon_coor, lat_coor):
    'Find partitions information in MFWAN datarmor files'
    
    date64 = datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
    dayYear = date64.strftime('%j')

    mfwam_data = pd.DataFrame()
    for dayYear in [str(int(dayYear)-1), dayYear, str(int(dayYear)+1)]:
        file2 = os.listdir(op.join(mfawm, '2021', dayYear))[0]
        mfwam_data = pd.concat([mfwam_data, pd.read_csv(
                op.join(mfawm, '2021', dayYear, file2),
                delimiter='\s+'
            )])
    try:
        mfwam_sel = mfwam_data.loc[(mfwam_data.L2name == SWIM_L2_NC_file_name)]
        min_point, min_dist = spectra.get_box(mfwam_sel.lon.values, mfwam_sel.lat.values, lon_coor, lat_coor)
        mfwam_sel = mfwam_sel.iloc[[min_point]]

        return(mfwam_sel)
    
    except:
        return(pd.DataFrame())
                
                
def compute_spectra(dsf, file, ds_ww3, beam, min_wavelength, max_wavelength):
    
    N_KI    = int(32)#int(65)
    N_PHI  = int(12)
    N_PARTITIONS = int(3)
    k_max= round(2*pi/min_wavelength,3)
    k_min= round(2*pi/max_wavelength,3)
    t0 = datetime.datetime(2009,1,1)
            
    ds_params_bulk = xr.Dataset()
    ds_params_freqs = xr.Dataset()
    ds_part_CFOSAT = xr.Dataset()
    
    NC_File_L2 = glob.glob(op.join(sat_cfosat,  '*', file))[0]
    cdf = netcdf.Dataset(glob.glob(op.join(sat_cfosat,  '*', file))[0])
        
    # Init variables
    time = cdf.variables['time_spec_l2'][:]
    lon = cdf.variables['lon_spec_l2'][:]
    lat = cdf.variables['lat_spec_l2'][:]
    K_SPECTRA = cdf.variables['k_spectra'][:]
    PARTI_BEAM = cdf.variables['mask'][:]
    PARTI_COMB = PARTI_BEAM[:,:,:,:,:,int((beam-6)/2)]
    PP_MEAN = cdf.variables['pp_mean'][:] 
    P_COMBINED = PP_MEAN[:,:,:,:,int((beam-6)/2)]

        
    for pbox, box in enumerate(dsf.nbox.values):

        # side of the nadir track to analyse (0 : right sides ; 1 :left side)
        for posneg in [0, 1]:
            dsfx = dsf.isel(nbox=pbox, posneg=posneg)

            # get dom_azi
            swh_part = cdf.variables['wave_param_part'][0,:,nadir_side,num_box,int((beam-6)/2)]
            dom_wl = cdf.variables['wave_param_part'][1,:,nadir_side,num_box,int((beam-6)/2)]
            dom_azi = cdf.variables['wave_param_part'][2,:,nadir_side,num_box,int((beam-6)/2)]
            
            if np.shape(swh_part.mask) == ():
                
                # Get nadir box time and coordinates
                lon_coor, lat_coor = lon[posneg, box], lat[posneg, box]
                date = (t0 + datetime.timedelta(seconds=float(time[posneg,box,0]))).strftime('%Y-%m-%dT%H:%M:%S')

                distance, bearing, bearing_p1 = spectra.geoBearing(dsfx.longitude.values,
                                                                   dsfx.latitude.values)
                
                # select mfwam partitions information (time, nadir_side, box, lon, lat)
                mfwam_sel = mfwam_info(date, posneg, box, lon_coor, lat_coor)

                # Remove the directional ambiguity
                dom_azi = dir_ambiguity(dom_azi, dom_wl, mfwam_sel, lon_coor, lat_coor)
       
                # 2D mean slope spectrum - to density spectrum
                # compute origial spectra from CFOSAT
                ds_Raw = spectra.rawSpectra(P_COMBINED, K_SPECTRA, dom_azi, 
                                    posneg=posneg, 
                                    box=box, n_k=N_KI, n_phi=N_PHI*2, 
                                    n_partitions=N_PARTITIONS)

                # eliminate 180º ambiguity
                ds_oneSide, mask_comb_or, mask_comb = spectra.oneSide(ds_Raw, K_SPECTRA, PARTI_COMB, dom_azi,
                                     posneg=posneg, box=box,
                                     n_k=N_KI,
                                     n_phi=N_PHI*2,
                                     n_partitions=N_PARTITIONS)

                # Select sector and compute mean parameters
                ds_spectra = ds_oneSide.sum(dim='part')

                ds_sector = ds_spectra.isel(theta=np.argmin(np.abs(ds_spectra.theta.values - bearing)))

                # Save the energy in frequency-direction bins
                ds_sector = ds_sector.assign(
                    {
                        'file': dsfx.file,
                        'nbox': dsfx.nbox,
                        'posneg': dsfx.posneg
                    }
                ).expand_dims(['file','nbox','posneg','theta'])

                dsfx = dsfx.expand_dims(['file', 'nbox', 'posneg', 'theta']).assign(
                    {
                        'bearing1':(['file', 'nbox', 'posneg'], [[[bearing_p1]]]),
                        'distance':(['file', 'nbox', 'posneg'], [[[distance]]])
                    }
                )
                dsi_freqs = xr.merge([dsfx, ds_sector])

                #ds_params_bulk = xr.merge([ds_params_bulk, dsi_bulk])
                ds_params_freqs = xr.merge([ds_params_freqs, dsi_freqs])

                del ds_Raw, ds_oneSide, ds_spectra
                gc.collect()

        del cdf, K_SPECTRA, PARTI_COMB, PARTI_BEAM, P_COMBINED, PP_MEAN
        gc.collect()
            
    return(ds_params_freqs)


def CFOSAT_partitions(ds_data, beam, min_wavelength, max_wavelength):
    'Extract partitions information from CFOSAT'
    
    for pfile, file in enumerate(ds_data.file.values):
        if pfile >= 137:
            sys.stdout.write('\r File number: {0}\{1}'.format(pfile,len(ds_data.file.values)))
            sys.stdout.flush()

            # Init resulting xr dataset
            ds_part_CFOSAT = xr.Dataset()        
            dsf = ds_data.sel(file=file).dropna(dim='nbox')

            NC_File_L2 = glob.glob(op.join(sat_cfosat,  '*', file))[0]
            cdf = netcdf.Dataset(NC_File_L2)

            # Init variables
            time = cdf.variables['time_spec_l2'][:]
            lon = cdf.variables['lon_spec_l2'][:]
            lat = cdf.variables['lat_spec_l2'][:]

            for pbox, box in enumerate(dsf.nbox.values):
                for posneg in [0, 1]:
                    dsfx = dsf.isel(nbox=pbox, posneg=posneg)

                    swh_part = cdf.variables['wave_param_part'][0,:,posneg,box,int((beam-6)/2)]
                    dom_wl = cdf.variables['wave_param_part'][1,:,posneg,box,int((beam-6)/2)]
                    dom_azi = cdf.variables['wave_param_part'][2,:,posneg,box,int((beam-6)/2)]

                    if np.shape(swh_part.mask) == ():

                        # Get nadir box time and coordinates
                        lon_coor, lat_coor = lon[posneg, box], lat[posneg, box]
                        date = (t0 + datetime.timedelta(seconds=float(time[posneg,box,0]))).strftime('%Y-%m-%dT%H:%M:%S')

                        distance, bearing, bearing_p1 = spectra.geoBearing(dsfx.longitude.values,
                                                                           dsfx.latitude.values)

                        # select mfwam partitions information (time, nadir_side, box, lon, lat)
                        mfwam_sel = mfwam_info(file, date, posneg, box, lon_coor, lat_coor)
                        
                        # No MFWAN file matching CFOSAT partition
                        if mfwam_sel.empty: break
                            
                        # Recompute the partitions considering the directional ambiguity
                        dom_azi = dir_ambiguity(dom_azi, dom_wl, mfwam_sel, lon_coor, lat_coor)

                        cell_text = spectra.partitions_info(cdf, box, posneg, beam)
                        cell_text['direction (°)'] = dom_azi
                        cell_text = cell_text.dropna()

                        # Save CFOSAT partitions information in xr Dataset
                        ds_cell_text = cell_text.set_index('partition').to_xarray()
                        ds_cell_text = ds_cell_text.assign_coords({'file':file,
                                                                   'posneg':posneg,
                                                                   'nbox':box})
                        ds_cell_text = ds_cell_text.expand_dims(['file', 'posneg', 'nbox'])
                        ds_part_CFOSAT = xr.merge([ds_part_CFOSAT, ds_cell_text])

            ds_part_CFOSAT.to_netcdf(op.join(p_output, 'process_CFOSATparts', 'F{0}_.nc'.format(pfile)))

    return(ds_part_CFOSAT)


def dir_ambiguity(dom_dir, dom_wl, ds_mfwam, lon_coor, lat_coor):
    '''
    Function to remove the 180º direction ambiguity considering the freq-dir energy peak of WW3 partitions
    Wave partitions are not neccesarilly ordered by decreasing energy TODO
    '''
    
    mdirs =  [ds_mfwam.mdww.values, ds_mfwam.mwd1.values, ds_mfwam.mwd2.values]
    mtms = [ds_mfwam.mpww.values, ds_mfwam.mwp1.values, ds_mfwam.mwp2.values]
    
    # From WW3 partitions, find the most similar value in SWIM
    # define a threshold for k and dir
    poswims = []
    for pang, ang in enumerate(mdirs):
        if np.isnan(ang) == False: # ww3 found partition
            success, pos_swim, flip = pos_find(dom_dir, dom_wl, mdirs[pang], mtms[pang]) # we found a SWIN associated partition 
            print(success, pos_swim, flip)
            poswims.append(pos_swim)
            if (success) & (flip==False): dom_dir[pos_swim] = mtms[pang]
            elif (success) & (flip): dom_dir[pos_swim] = dom_dir[pos_swim] + 180
    
    # positions not found
    posnans = [i for i in range(len(dom_dir)) if i not in poswims]
    dom_dir[posnans] = np.nan

    return(dom_dir)


def pos_find(dom, dom_wlg, dirw, tpw):
    'Define thresholds (Wavelength, wavenumber and direction) and look for similar values'
    thL = 50 
    thk = 2*np.pi/thL
    thd = 40
    
    # from Tp to L
    lpw = (g*tpw**2)/(2*np.pi)
    
    # First filter with wavelength
    minl = np.abs(dom_wlg[np.argmin(np.abs(dom_wlg - lpw))] - lpw)

    if minl <= thL:
        # angular difference
        value = dom[np.argmin(np.abs(dom_wlg - lpw))]

        mind = np.abs(get_distance(value, dirw))
        mind1 = np.abs(get_distance(value+180, dirw))

        if mind <= thd:
            return(True, np.argmin(np.abs(dom_wlg - lpw)), False)
        elif mind1 <= thd:
            return(True, np.argmin(np.abs(dom_wlg - lpw)), True)
        else:
            return(False, np.nan, False)
    else:
        return(False, np.nan, False)
    
    
def get_distance(unit1, unit2):
    phi = abs(unit2-unit1) % 360
    sign = 1
    # used to calculate sign
    if not ((unit1-unit2 >= 0 and unit1-unit2 <= 180) or (
            unit1-unit2 <= -180 and unit1-unit2 >= -360)):
        sign = -1
    if phi > 180:
        result = 360-phi
    else:
        result = phi

    return result*sign