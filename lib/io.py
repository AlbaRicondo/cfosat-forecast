#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import os.path as op

# arrays
import numpy as np
import pandas as pd
import xarray as xr
import datetime

# kml read
from pykml import parser

# dependencies
from lib.config import *

def isin_estela(lon, lat, estela_mask):
    'Return boolean var True False if is contained in the ESTELA'
    val = estela_mask.isel(
        longitude=np.argmin(np.abs(estela_mask.longitude.values-lon)),
        latitude=np.argmin(np.abs(estela_mask.latitude.values-lat)),
    ).values
    
    if val == 1: return(True)
    else: return(False)
    
def load_files(files):
    '''Extract last days of CFOSAT tracks'''

    df_data = pd.DataFrame()
    df_ini, df_fini, df_names = [], [], []
    for p, i in enumerate(files):
        file = files[p].split('.nc">')[0]
        df_names.append(file)
        init = file[22:37]
        fini = file[38:-3]
        dates_init = np.datetime64(pd.to_datetime(init))
        dates_fini = np.datetime64(pd.to_datetime(fini))
        df_ini.append(dates_init)
        df_fini.append(dates_fini)

    df_data['name'] = df_names
    df_data['dates_st'] = df_ini
    df_data['dates_ed'] = df_fini 
    
    return(df_data)

def load_timelags(estela_mask):
    '''Read passess offset time'''
    
    with open(file_timelag) as f:
        root = parser.parse(f).getroot().Document.Folder

    array_lags, array_coords, offs = [], [], []
    for pm in root.Placemark:
        point = [p for p in pm.getchildren() if p.tag.endswith('Point')]
        if point:
            coords = point[0].coordinates.text
        else:
            poly = [p for p in pm.getchildren() if p.tag.endswith('Polygon')]
            if poly:
                coords = poly[0].outerBoundaryIs.LinearRing.coordinates.text

        lon, lat = np.float(coords.split(",")[0]), np.float(coords.split(",")[1])
        #if lon > 0: lon = lon - 360
        if isin_estela(lon, lat, estela_mask):
        #if ((lon > lon1_P) & (lon < lon2_P) & (lat < lat2_P) & (lat > lat1_P)):
            array_coords.append(coords.split(",")[:2])
            array_lags.append(pm.name)
            offs.append(datetime.timedelta(days=np.int(str(pm.name)[3:5]), 
                                               hours=np.int(str(pm.name)[7:9]), 
                                               minutes=np.int(str(pm.name)[10:12])))
            
    return(offs)

def load_tracks():
    '''Obtaining the pass over dates by adding the time-lags'''
    
    tracks = pd.read_csv(
        file_tracks, 
        header=None, 
        delimiter=';', 
        names=['cycle', 'RE', 'time'],
        infer_datetime_format = True,
        parse_dates = True,
        keep_date_col = True,
        date_parser = lambda x: pd.to_datetime(x, format="%Y%m%d %H:%M:%S"), 
        index_col="time"
    )
    tracks = tracks.loc[tracks.index >= datetime.datetime.now()-datetime.timedelta(days=10*n_days)]
    tracks = tracks.loc[tracks.index < datetime.datetime.now()]
    
    return(tracks)
