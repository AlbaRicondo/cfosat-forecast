spectra:         compute single spectra 2D
wrapSpectra:     process the spectral data in global tracks

io:              input output CFOSAT tracks

sat:             extract parameters from satellite tracks
plot_sat:        plot tracks parameters and forecast results

model