#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as op
import sys
import glob

import numpy as np
import pandas as pd
import datetime
import xarray as xr
from datetime import timedelta

import warnings
warnings.filterwarnings('ignore')

# dependencies
from lib.config import *

def hindcast_ndays(array_vars):
    'Select last ndays files from WW3 global grid'
    
    files = glob.glob(op.join(ww3_path, '*'))
    name, time = [], []
    ds_ww3 = xr.Dataset()

    for p, i in enumerate(files):
        date = files[p].split('WW3-GLOB-30M_')[1][:-3]
        date = np.datetime64(pd.to_datetime(date))
        if (date > datetime.datetime.now() - datetime.timedelta(days=n_days)) & (date <= datetime.datetime.now()):
            dst = xr.open_dataset(i)[array_vars]
            ds_ww3 = xr.merge([ds_ww3, dst])
            name.append(i)
            time.append(date)

    df_ww3 = pd.DataFrame(
        {
            'name':name,
            'date':time
        }
    ) 
    return(df_ww3)

def spec_st_ndays(st_name):
    'Select following days files from WW3 spectral station'
    
    # input spectral file
    path = op.join(ww3_spectra_path, st_name)
    files = glob.glob(op.join(path, '*'))

    ds_ww3 = xr.Dataset()

    for p, i in enumerate(files):
        date = files[p].split('MARC_WW3-GLOB-30M-{0}_'.format(st_name))[1][:-8]
        date = np.datetime64(pd.to_datetime(date))
        if (date > datetime.datetime.now()):
            dst = xr.open_dataset(i)
            ds_ww3 = xr.merge([ds_ww3, dst])
    
    return(ds_ww3)
            
    
    





