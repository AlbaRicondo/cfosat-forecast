#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
plot_spectre_2D_casys.py
:author: A. Jouzeau, M. Dalila
:creation date : 09-10-2017
:last modified : 08-11-2019

python 2.7 test OK python 3.X pas de test
Copyright 2017, CLS.

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
General Public License for more details (http://www.gnu.org/licenses/).
"""
import os
import os.path as op
import sys

from math import pi
import numpy as np
import argparse
import netCDF4 as netcdf
import datetime
import xarray as xr
import pandas as pd
from scipy.interpolate import griddata

from matplotlib import pyplot as plt
from matplotlib.patches import Patch
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MaxNLocator

# cartopy maps
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature

from lib import plot_sat
from .config import *


def pol2cart(rho, phi):
    """
    Polar => Carthesian
    Usage:
        (x,y) = pol2cart(rho, phi)
    With:
        :param      rho     :distance to center     :NA     :NA     :E
        :param      phi     :angle                  :NA     :NA     :E
        :type       rho     :float
        :returns    (x,y)   :coordinates in carthesian
        :rtype      (x,y)   :tuple of floats
    """
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)



def plot_polarfig(mask, kspectra, array_color, n_phi, title, lmin, lmax, 
                  ax=None, fig=None, vmin=None, vmax=None, disp_partitions=True):
    """
    Plot one 2D spectrum against wave number vector
    Usage:
        plot_polarfig(mask, k_spectra, array_color,
                      n_phi, title, disp_mask)
    With:
        :param mask         :detected partitions            :NA :NA         :E
        :param kspectra     :wave number vector             :L2 :k_spectra  :E
        :param array_color  :2D spectrum                    :NA :NA         :E
        :param n_phi        :number of azimuthal  angle     :L2 :n_phi      :E
        :param title        :number of azimuths             :NA :NA         :E
        :param disp_mask    :display or not masks           :NA :NA         :E
        :type mask         :numpy.ma
        :type kspectra     :float
        :type array_color  :float
        :type n_phi        :int
        :type title        :string
        :type disp_mask    :boolean
        :returns    None
        :rtype      None    :NA
    """
    
    if ax is None:
        fig, ax = plt.subplots(facecolor='white', figsize=(11, 9))
    else:
        fig = plt.gcf()
    
    # size of wave number vector
    n_k = len(kspectra[:])
    # polar coordinates
    r = np.tile(kspectra[:], n_phi+1).reshape(n_phi+1, n_k)
    # grid , 25 = n_pĥi +1
    theta, _ = np.mgrid[0.0:2.0*pi:25j, 0:n_k:1]
    
    x = theta
    y = r 
    
    # Y-axis from wavenumber k to wavelength L
    yL = 2*np.pi/r
    
    # Spectrum map
    pc = ax.pcolormesh(x, yL, np.ma.filled(array_color, fill_value=np.nan),
                       cmap='rainbow', vmin=vmin, vmax=vmax, zorder=1) 
    

    # radius labels locations
    #labels = [30, 50, 100, 200, 400]
    #ax.set_rticks([2*pi/i for i in labels])
    #labels = [str(i) + ' m' for i in labels]
    #ax.yaxis.set_ticklabels(labels)
    ax.set_theta_offset(np.pi/2)
    ax.set_theta_direction(-1)
    ax.yaxis.set_major_locator(MaxNLocator(4)) 
    ax.set_rlabel_position(45)
    #if original:
    #ax.set_xticks([i*pi/4 for i in range(8)])
    #ax.set_xticklabels(['90','45','0','315','270','225','180','135'])
        #ax.set_theta_direction(-1)
    #else:
    #    ax.set_theta_offset(np.pi/2)
        #ax.set_theta_direction(-1)
    
    ax.yaxis.set_major_locator(MaxNLocator(4)) 
    #ax.set_theta_zero_location('N', offset=np.pi/2)
    #ax.set_theta_offset(np.pi/2)
    #ax.set_theta_direction(-1)

    # radius labels in white
    ax.tick_params(labelcolor='k', axis='y')
    ax.set_rlim(bottom=lmax, top=lmin)
    #ax.set_rmax(0.21)

    # theta ticks
    #ax.set_xticks([i*pi/6 for i in range(12)])
    #ax.set_xticklabels(['90','60','30','0','330','300','270','240','210','180','150','120'])
    
    #ax.set_xticks([i*pi/4 for i in range(8)])
    #ax.set_xticklabels(['90','45','0','315','270','225','180','135'])

    # add Partitions edges
    if disp_partitions:
        ax.contour(x[:-1,:], yL[:-1,:], mask[0, :, :],
                    levels=[0.999],
                    colors='#FF0E00',
                    origin='upper',
                    linewidths=2.0,
                    linestyles='dashed',
                    label='partition 1', zorder=2)
        ax.contour(x[:-1,:], yL[:-1,:], mask[1, :, :],
                    levels=[0.999],
                    #colors='#FFFFFF',
                    colors='w',
                    origin='upper',
                    linewidths=1.0,
                    linestyles='solid',
                    label='partition 2', zorder=3)
        ax.contour(x[:-1,:], yL[:-1,:], mask[2, :, :],
                    levels=[0.999],
                    colors='#D4FF9C',
                    origin='upper',
                    linewidths=2.0,
                    linestyles='dashdot',
                    label='partition 3', zorder=4)
         
    ax.set_title(title)
    ax.grid(False)
    
    return(pc)


def plotSpectra(fig, axt, wave_spec, mask_comb, K_SPECTRA, 
            box, posneg,
            N_PHI, k_min, k_max,
            vmin, vmax, partitions,
            lon_coor, lat_coor, date):
    
    title = '2D mean slope spectrum, for box: ' + str(box) + ', posneg: '+ str(posneg)
    suptitle = '\nCoordinates: {0:.2f}°, {1:.2f}°\nDate: {2}'.format(lon_coor, lat_coor, date)
    title += suptitle
    
    pc = plot_polarfig(mask_comb, K_SPECTRA, wave_spec, N_PHI*2, title, k_min, k_max, 
                       ax=axt, fig=None, vmin=vmin, vmax=vmax, disp_partitions=partitions)
    
    p1 = Patch(edgecolor='#FF0E00',linestyle='dashed',
                   linewidth=1.0, fill=False)
    p2 = Patch(edgecolor='#FFFFFF', linestyle='solid',
                   linewidth=1.0, fill=False)
    p3 = Patch(edgecolor='#D4FF9C',linestyle='dashdot',
                       linewidth=1.0, fill=False)
    
    legend = axt.legend([p1, p2, p3],
                       ['Partition 1', 'Partition 2', 'Partition 3'],
                       loc='best')
    
    frame = legend.get_frame()
    frame.set_facecolor('black')
    for text in legend.get_texts():
        plt.setp(text, color = 'w') 

    # add colorbar
    #divider = make_axes_locatable(axt)
    #axc = divider.new_horizontal(size="%3", pad=0.1, axes_class=plt.Axes)
    
   # axc, loc = plot_sat.add_colorbar(axt)
    #axc = fig.add_axes([0.5, 0.3, 0.025, 0.4])
    #cbar = fig.colorbar(pc, cax=axc)
    
    return(fig, axt)

def plotModel(fig, axt, spec, cmap, title, vmin, vmax, kmin, kmax):
    'Plot polar spectra form WW3 model. Convert direction from going TO to coming FROM'

    #spec = spec.sortby('frequency', ascending=False)
    
    # eliminate row,cell dimensions
    x = spec.direction
    y = spec.frequency
    z_density = spec.ww3_efth
    
    # 
    z_slope = z_density * 2*np.pi/(9.806/(2*np.pi*spec.frequency**2)) # Z slope = Z density * k
    
    # frequencies: center frequency bin
    
    # Y-axis from wavenumber k to wavelength L
    yL = g/(2*np.pi*y**2)
    
    # Flip direction
    xc = []
    for pd, dirt in enumerate(x):
        if dirt < 180: xc.append(dirt + 180)
        else: xc.append(dirt - 180)
    
    xc = np.deg2rad(xc)
    xc = np.append(xc, xc[0])
    z1 = np.column_stack((z_slope[:,:],z_slope[:,-1]))
    
    pc = axt.pcolormesh(xc, yL, np.sqrt(z1), vmin=vmin, vmax=vmax) 
    
    #axt.set_rlim(kmin,kmax)
    axt.set_rlim(bottom=kmax, top=kmin)
    pc.set_cmap(cmap)
    axt.set_title(title)
    axt.set_theta_offset(np.pi/2)
    axt.set_theta_direction(-1)
    axt.yaxis.set_major_locator(MaxNLocator(4)) 

    # add colorbar
    #axc, loc = plot_sat.add_colorbar(axt)
    axc = fig.add_axes([1, 0.3, 0.025, 0.4])
    fig.add_axes(axc)
    cbar = fig.colorbar(pc, cax=axc)
    
    return(fig, axt)
    

def plotPartitions(fig, gs, wave_spec, mask_comb, K_SPECTRA, 
                  box, posneg,
                  N_PHI, k_min, k_max,
                  vmin, vmax, partitions,
                  lon_coor, lat_coor, date):

    title = '2D mean slope spectrum, for box: ' + str(box) + ', posneg: '+ str(posneg)
    suptitle = '\nCoordinates: {0:.2f}°, {1:.2f}°\nDate: {2}'.format(lon_coor, lat_coor, date)
    title += suptitle
    
    p1 = Patch(edgecolor='#FF0E00',linestyle='dashed',
                   linewidth=1.0, fill=False)
    p2 = Patch(edgecolor='k', linestyle='solid',
                   linewidth=1.0, fill=False)
    p3 = Patch(edgecolor='#D4FF9C',linestyle='dashdot',
                       linewidth=1.0, fill=False)
    
    # legend settings
    patchs = [p1, p2, p3]
    labels = ['Partition 1', 'Partition 2', 'Partition 3']
    
    # plot energy partitions
    for p, part in enumerate(wave_spec.part.values):
        ax = fig.add_subplot(gs[0, p], projection='polar')
        print(wave_spec.sel(part=part).slopeD.values)
        pc = plot_polarfig(mask_comb, K_SPECTRA, wave_spec.sel(part=part).slopeD, N_PHI*2, title, k_min, k_max, 
                          ax=ax, fig=fig, 
                          vmin=vmin, vmax=vmax, disp_partitions=False)
        legend = ax.legend([labels[p]],
                       [labels[p]],
                       loc='best')
        
        #frame = legend.get_frame()
        #frame.set_facecolor('black')
        
        #for text in legend.get_texts():
            #plt.setp(text, color = 'w') 
            
        ax.set_title('Partition {0}'.format(int(p+1)))
        
    # add colorbar
    axc = fig.add_axes([1, 0.35, 0.025, 0.35])
    cbar = fig.colorbar(pc, cax=axc)
    
    return(fig)

def grid(x, y, z):
    "Convert 3 column data to matplotlib grid"
    # avoid interpolation break due to +180/-180º parallel
    x = np.where(x<0, 360-np.abs(x), x)
    xi = np.linspace(min(x), max(x), 100)
    yi = np.linspace(min(y), max(y), 100)
    
    xy = pd.DataFrame({'x':x, 'y':y}).values
    
    X, Y = np.meshgrid(xi, yi)
    Z = griddata(xy, z, (X, Y), )
    
    return X, Y, Z

def addmap(ax):
    'Cartopy map customize'
    ax.patch.set_facecolor('cornflowerblue')
    ax.patch.set_alpha(0.7)
    ax.add_feature(shape_feature_l)