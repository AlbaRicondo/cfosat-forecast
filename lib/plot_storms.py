#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as op
import sys

from math import pi
import numpy as np
import argparse
import netCDF4 as netcdf
import datetime
import xarray as xr
import pandas as pd
from scipy.interpolate import griddata

from matplotlib import pyplot as plt
from matplotlib.patches import Patch
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MaxNLocator
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

# cartopy maps
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature

from lib import plot_sat
from .config import *

def colormap(lim_up, lim_down):
    """
    Custom colormap from 2 user python colorbars
    """
    
    colors1, colors2 = 'jet', 'jet'
    bottom = cm.get_cmap(colors2, lim_up)
    top = cm.get_cmap(colors1, lim_down)
    newcolors = np.vstack((bottom(np.linspace(0, 0.8, lim_up)),
                       top(np.linspace(0.1, 1, lim_up))))
    
    # linear increasing alpha
    newcolors[:,-1] = np.linspace(0, 1, len(newcolors))
    
    return(ListedColormap(newcolors))



